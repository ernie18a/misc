[[_TOC_]]
### directory hierarchy
```bash
[C23011-ErnieHo ~/.G/misc main] $ tree -dL 1
.
├── examples
└── notes
```
### 初始化
```bash
source /dev/stdin <<< "$(curl -Ls https://gitlab.com/ernie18a/dotfiles/-/raw/main/home/.bash_profile)"
```
